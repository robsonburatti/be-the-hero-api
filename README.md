# How to clone de project and start the container docker

## Clone the project

```console
git clone git@gitlab.com:buratti-experiments/omni-stack-11/be-the-hero-api.git
```

## Install the packages of dependencies of the project

```console
npm i
```

## Create image docker locale

```console
docker build -t registry.gitlab.com/buratti-experiments/omni-stack-11/be-the-hero-api .
```

## Run container docker

```console
docker run -it -p 9000:3000 -v $(pwd):/src/server registry.gitlab.com/buratti-experiments/omni-stack-11/be-the-hero-api
```

---

# Create project with structure node

```console
npm init -y
```

# Install the packages of dependencies of the your project

```console
npm i
```

# Create the file `Dockerfile` with the configurations

```yaml
FROM node:9-slim
WORKDIR /src/server
COPY package.json /src/server
RUN npm install
COPY . /app
CMD [ "npm", "start" ]
```

# Create de image docker

```console
docker build -t <project-name-here> .
```

# Run your container docker

```console
docker run -it -p 9000:3000 -v $(pwd):/src/server <project-name-here>
```

# Project configuration scripts

## Settings of the execution environments

### Create the file with the settings of the project execution environments

```console
npx knex init
```

### How to run the database creation script

```console
npx knex migrate:make create_incidents
```

```console
npx knex migrate:make create_ongs
```

### How to know the creation status of the base creation scripts

```console
npx knex migrate:status
```

### How to run the migration scripts

```console
npx knex migrate:latest
```

## How to run the test

### How to run the test database creation script

#### Adjust in the configuration file of execution environments created with `knex` then it is necessary to execute the command to create the test environment

```console
npx knex migrate:latest
```

### Create the file with the settings executing the project tests

```console
npx jest --init
```

### How to run the test scripts

```console
npm test
```
